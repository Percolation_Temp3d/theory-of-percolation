// Copyright 2020-2022 Taras Yaitskyi

#ifdef _WIN32
#include <windows.h>
#endif

#include "gui.h"
#include "src/tester.h"

int main() {
#ifdef _WIN32
  ShowWindow(GetConsoleWindow(), SW_HIDE);
#endif

  Fl::scheme("gtk+");
  Fl::gl_visual(FL_RGB | FL_ALPHA);

  Application app;
  app.entry_window->show();

  return Fl::run();
}
