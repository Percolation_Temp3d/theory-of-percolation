#ifndef SRC_MAIN_WINDOW_H_
#define SRC_MAIN_WINDOW_H_

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>

namespace FLTK_graphics {
class MainWindow : public Fl_Double_Window {
 public:
  MainWindow(int W, int H, const char* L) : Fl_Double_Window(W, H, L) {}

  void hide();
};
}  // namespace FLTK_graphics

#endif  // SRC_MAIN_WINDOW_H_
