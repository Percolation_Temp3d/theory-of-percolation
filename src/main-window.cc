#include "src/main-window.h"

#include <cstdlib>

namespace FLTK_graphics {
void MainWindow::hide() { exit(EXIT_SUCCESS); }
}  // namespace FLTK_graphics
