﻿#include "src/path-finder.h"

#include <cassert>
#include <cstddef>
#include <list>
#include <stack>
#include <thread>
#include <utility>

#include "constants.h"
#include "src/smartslices.h"

namespace analyzer {
typedef unsigned int uint;

using std::list;
using std::pair;
using std::stack;
using std::thread;
using std::vector;

// Constructor initializer
GraphCalculator::GraphCalculator(coorscope scope, vector<point> &filled,
                                 uint concentration, bool pos)
    : adjacency_matrix_(filled) {
  adjacency_matrix_ = filled;
  any_possible_full_path_ = pos;

  this->concentration_ = concentration;
  x_scope_ = scope.x_max;
  y_scope_ = scope.y_max;
  z_scope_ = scope.z_max;
}

void GraphCalculator::ClusterRevisit() {
  /*
  After DFS or BFS was ran, we should make all points unvisited again.
  */
  size_t island_len = cluster_ways_.size();
  for (size_t i = 0; i < island_len; ++i) {
    vector<point *> wat = cluster_ways_[i];
    size_t inner = wat.size();

    for (size_t in = 0; in < inner; ++in) {
      point *linked = wat[in];
      linked->args.visited = false;
    }
  }
}

void GraphCalculator::UnVisited() {
  /*
  After DFS or BFS was ran, we should make all points unvisited again.
  */
  for (size_t i = 0; i < z_scope_; ++i) {
    for (size_t j = 0; j < x_scope_; ++j) {
      for (size_t last = 0; last < y_scope_; ++last) {
        set_el(adjacency_matrix_, i, j, last, x_scope_, y_scope_).args.visited =
            false;
      }
    }
  }
}

void GraphCalculator::CreateMatrix(bool should_be_more) {
  /*
  This function fills adjacency_matrix_ variable.
  The process divided by height layers - every thread owns its unique scope of
  points
  */
  vector<thread> threads;

  const auto processor_count = thread::hardware_concurrency();

  size_t real = processor_count;
  if (!should_be_more) {
    real = 1;
  }

  size_t n = z_scope_ / real, query = z_scope_ % real;
  if (n == 0 || n == 1) {
    n = z_scope_;
    real = 1;
  } else if (query != 0) {
    n += query;
  }

  for (size_t i = 1; i <= real; ++i) {
    threads.emplace_back([&, i]() {
      for (size_t z = n * (i - 1); z < n * i && z < z_scope_; ++z) {
        for (size_t x = 0; x < x_scope_; ++x) {
          // Here is a check if a layer has any conductor in it
          if (!get_el(adjacency_matrix_, z, static_cast<size_t>(0),
                      static_cast<size_t>(0), x_scope_, y_scope_)
                   .args.z_layer_metric ||
              !get_el(adjacency_matrix_, z, x, static_cast<size_t>(0), x_scope_,
                      y_scope_)
                   .args.x_layer_metric) {
            continue;
          }

          for (size_t y = 0; y < y_scope_; ++y) {
            size_t sz, sx, sy;

            if (!get_el(adjacency_matrix_, z, x, y, x_scope_, y_scope_)
                     .args.is_conductor) {
              continue;
            }
            // This reserves memory if concentration_ is big enough so it
            // would be faster to push_back element in the vector
            if (concentration_ >= 50) {
              set_el(adjacency_matrix_, z, x, y, x_scope_, y_scope_)
                  .near_.reserve(BestNToReserve());
            }

            sz = 0;
            if (z == 0) {
              sz++;
            }

            // 3 Loops for every axes to cover all points neighbors
            for (sz += z - 1; sz <= (z + 1) && sz < z_scope_; ++sz) {
              sx = 0;
              if (x == 0) {
                sx++;
              }

              for (sx += x - 1; sx <= (x + 1) && sx < x_scope_; ++sx) {
                sy = 0;
                if (y == 0) {
                  sy++;
                }

                for (sy += y - 1; sy <= (y + 1) && sy < y_scope_; ++sy) {
                  if ((z != sz || x != sx || y != sy) &&
                      get_el(adjacency_matrix_, sz, sx, sy, x_scope_, y_scope_)
                          .args.is_conductor) {
                    set_el(adjacency_matrix_, z, x, y, x_scope_, y_scope_)
                        .near_.push_back(&set_el(adjacency_matrix_, sz, sx, sy,
                                                 x_scope_, y_scope_));
                  }
                }
              }
            }
          }
        }
      }
    });
  }

  for (size_t i = 0; i < real; ++i) {
    threads[i].join();
  }
}

uint GraphCalculator::BestNToReserve() const {
  // This function calculates how much memory should I reserve for the near_
  // vector
  uint n_reserved;

  if (concentration_ <= 80) {
    n_reserved = 17;  // all around + corner
  } else {
    n_reserved = 26;  // all around
  }

  return n_reserved;
}

int GraphCalculator::AroundSummarizeAdjuster() const {
  if (concentration_ >= 95) {
    return 3;
  } else if (concentration_ > 50) {
    return 4;
  }

  return 6;
}

int GraphCalculator::AroundSummarize(point *grp) const {
  // This function helps to calculate partition area around the current point
  int aroundssum = 0;
  uint hype = 0, closure = 0, z = grp->pwp.z, y = grp->pwp.y, x = grp->pwp.x;

  if (z == 0 || z == (z_scope_ - 1)) {
    hype++;
  }
  if (x == 0 || x == (x_scope_ - 1)) {
    hype++;
  }
  if (y == 0 || y == (y_scope_ - 1)) {
    hype++;
  }

  if (z_scope_ == 1) {
    closure++;
  }
  if (y_scope_ == 1) {
    closure++;
  }
  if (x_scope_ == 1) {
    closure++;
  }

  switch (closure) {
    case 0:
      switch (hype) {
        case 0:
          aroundssum = constants::kMax3DNeighborsCount;
          break;
        case 1:
          aroundssum = constants::kMiddle3DNeighborsCount;
          break;
        case 2:
          aroundssum = constants::kLow3DNeighborsCount;
          break;
        case 3:
          aroundssum = constants::kMinNeighborsCount;
          break;
      }
      break;
    case 1:
      switch (hype) {
        case 1:
          aroundssum = constants::kMax2DNeighborsCount;
          break;
        case 2:
          aroundssum = constants::kMiddle2DNeighborsCount;
          break;
        case 3:
          aroundssum = constants::kMin2DNeighborsCount;
          break;
      }
      break;
    case 2:
      if (hype == 2)
        aroundssum = constants::kMax1DNeighborsCount;
      else
        aroundssum = constants::kMin1DNeighborsCount;
      break;
    default:
      assert("omg da WTF?");
  }

  // This should be fixed. Its nice hotfix but why and for what it stands
  // idk...
  // TODO(TarasYa): (this is going to happen absolutely not soon): fix and
  // plz know why, why do I need this
  aroundssum -= grp->near_.size();
  if (aroundssum < 0) {
    aroundssum = 0;
  }

  return aroundssum / AroundSummarizeAdjuster();
}

uint GraphCalculator::CalcSBetween() {
  // Function to calculate S
  ClusterRevisit();
  int result_S = 0;

  size_t island_len = cluster_ways_.size();
  for (size_t i = 0; i < island_len; ++i) {
    vector<point *> wat = cluster_ways_[i];
    size_t inner = wat.size();

    for (size_t in = 0; in < inner; ++in) {
      point *linked = wat[in];
      if (!linked->args.visited) {
        result_S += static_cast<uint>(AroundSummarize(linked));
        linked->args.visited = true;
      }
    }
  }

  return result_S;
}

void GraphCalculator::ViaDFS_util(bool clear) {
  // DFS algorithm
  // Check if it should be cleared, may use it for some speed tests
  if (clear) {
    ClearFS();
  }

  stack<point> roots;
  size_t i = 0;

  for (size_t z = 0; z < z_scope_; ++z) {
    if (!get_el(adjacency_matrix_, z, static_cast<size_t>(0),
                static_cast<size_t>(0), x_scope_, y_scope_)
             .args.z_layer_metric) {
      continue;
    }
    for (size_t x = 0; x < x_scope_; ++x) {
      if (!get_el(adjacency_matrix_, z, x, static_cast<size_t>(0), x_scope_,
                  y_scope_)
               .args.x_layer_metric) {
        continue;
      }
      for (size_t y = 0; y < y_scope_; ++y) {
        point &head = set_el(adjacency_matrix_, z, x, y, x_scope_, y_scope_);
        if (head.args.visited || !head.args.is_conductor) {
          continue;
        }

        cluster_ways_.emplace_back(vector<point *>());
        cluster_ways_[i].push_back(&head);

        set_el(adjacency_matrix_, z, x, y, x_scope_, y_scope_).args.visited =
            true;
        roots.push(head);

        while (!roots.empty()) {
          point node = roots.top();
          roots.pop();

          size_t vlen = node.near_.size();
          for (size_t el_i = 0; el_i < vlen; ++el_i) {
            point *tail = node.near_[el_i];

            if (tail->args.visited) {
              continue;
            }
            tail->args.visited = true;

            cluster_ways_[i].push_back(tail);
            roots.push(*tail);
          }
        }

        ++i;
      }
    }
  }

  UnVisited();
}

void GraphCalculator::FindDeIsland(bool clear) {
  // BFS parallel algorithm
  // Check if it should be cleared, may use it for some speed tests
  if (clear) {
    ClearFS();
  }

  list<point *> queue;
  size_t i = 0;

  for (size_t z = 0; z < z_scope_; ++z) {
    if (!get_el(adjacency_matrix_, z, static_cast<size_t>(0),
                static_cast<size_t>(0), x_scope_, y_scope_)
             .args.z_layer_metric) {
      continue;
    }
    for (size_t x = 0; x < x_scope_; ++x) {
      if (!get_el(adjacency_matrix_, z, x, static_cast<size_t>(0), x_scope_,
                  y_scope_)
               .args.x_layer_metric) {
        continue;
      }
      for (size_t y = 0; y < y_scope_; ++y) {
        point &head = set_el(adjacency_matrix_, z, x, y, x_scope_, y_scope_);
        if (head.args.visited || !head.args.is_conductor) {
          continue;
        }

        set_el(adjacency_matrix_, z, x, y, x_scope_, y_scope_).args.visited =
            true;
        queue.push_back(&head);
        cluster_ways_.emplace_back(vector<point *>());

        while (!queue.empty()) {
          point *cur;
#pragma omp critical
          {
            cur = queue.front();
            cluster_ways_[i].push_back(cur);
            queue.pop_front();
          }

          size_t adjacency_len = cur->near_.size();
#pragma omp parallel for
          for (size_t el_i = 0; el_i < adjacency_len; ++el_i) {
            point *tail = cur->near_[el_i];

            if (tail->args.visited) {
              continue;
            }
            tail->args.visited = true;

#pragma omp critical
            queue.push_back(tail);
          }
        }

        ++i;
      }
    }
  }

  UnVisited();
}

void GraphCalculator::FindDeFull(bool clear) {
  // Creates vector (perforating_ways_) of full (through) clusters
  // Check if it should be cleared, may use it for some speed tests
  if (clear) ClearDFS();

  if (any_possible_full_path_) {
    size_t island_len = cluster_ways_.size();

    for (size_t i = 0; i < island_len; ++i) {
      vector<point *> *wat = &cluster_ways_[i];
      size_t inner = wat->size();

      if (inner < z_scope_) {
        continue;
      }
      pair<bool, bool> flag;
      for (size_t in = 0; in < inner; ++in) {
        point *cur = wat->at(in);

        if (cur->pwp.z == 0) {
          flag.first = true;
        }
        if (cur->pwp.z == (z_scope_ - 1)) {
          flag.second = true;
        }

        size_t from_end = inner - in - 1;
        if (from_end < inner) {
          if (wat->at(from_end)->pwp.z == z_scope_ - 1) {
            flag.second = true;
          }
        }

        if (flag.first && flag.second) {
          for (size_t j = 0; j < inner; ++j) {
            point *temp_pc = wat->at(j);
            temp_pc->args.through_out = true;
          }
          perforating_ways_.push_back(wat);

          break;
        }
      }
    }
  }
}

size_t GraphCalculator::CalcMidNeighborsN() {
  if (adjacency_matrix_.empty()) {
    return 0;
  }

  size_t sum = 0, mid = 0, n_needed = 0, mas_len = adjacency_matrix_.size();
  for (size_t i = 0; i < mas_len; ++i) {
    point ll = adjacency_matrix_[i];

    if (ll.args.is_conductor) {
      sum += ll.near_.size();
      ++n_needed;
    }
  }

  if (n_needed != 0) {
    mid = sum / n_needed;
  }

  return mid;
}

double GraphCalculator::CalcMidFractalIndex() {
  size_t data_len = cluster_ways_.size();

  if (data_len != 0) {
    double fractal_sum = 0, mid_fractal_index;

    for (size_t i = 0; i < data_len; ++i) {
      const std::vector<point *> &ref_to_specific_island = cluster_ways_[i];
      fractal_sum += CalcFractalIndexPerOne(ref_to_specific_island);
    }

    mid_fractal_index = fractal_sum / data_len;

    return mid_fractal_index;
  }

  return 0;
}

void GraphCalculator::ClearFinal() { adjacency_matrix_.clear(); }

void GraphCalculator::ClearFS() { cluster_ways_.clear(); }

void GraphCalculator::ClearDFS() { perforating_ways_.clear(); }

bool GraphCalculator::get_n_deep_path() const {
  return any_possible_full_path_;
}

vector<point> &GraphCalculator::get_final_paths() const {
  return adjacency_matrix_;
}

vector<vector<point *>> &GraphCalculator::get_bfs() { return cluster_ways_; }

vector<vector<point *> *> &GraphCalculator::get_dfs() {
  return perforating_ways_;
}
}  // namespace analyzer
