﻿#ifndef SRC_RANDOMIZER_H_
#define SRC_RANDOMIZER_H_

#include <cstddef>
#include <vector>

#include "src/point.h"

namespace analyzer {

class Randomize {
 private:
  size_t V_;
  bool seed_;

  std::vector<size_t> indexes_;
  std::vector<bool> choices_;

 public:
  explicit Randomize(size_t v_figure, bool seed = false);

  void AddRandomize(size_t count, bool shf = true);
  void NullRandomize();
  void Shuffle();

  std::vector<bool> &get_3D_to_1D();
};
}  // namespace analyzer

#endif  // SRC_RANDOMIZER_H_
