﻿#ifndef SRC_TESTME_H_
#define SRC_TESTME_H_

#include <string>

namespace tester {
class ITest {
 public:
  virtual std::string Log() = 0;
};
}  // namespace tester

#endif  // SRC_TESTME_H_
