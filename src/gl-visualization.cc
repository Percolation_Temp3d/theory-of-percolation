#include "src/gl-visualization.h"

#include <FL/Fl.H>
#include <FL/gl.h>
#include <FL/glu.h>
#include <FL/glut.H>

#include <cmath>
#include <string>
#include <vector>

#include "src/constants.h"
#include "src/point.h"

using std::string;
using std::vector;

namespace FLTK_graphics {
typedef unsigned int uint;

void ChooseColor(const analyzer::nomem &pp) {
  if (pp.args.through_out) {
    glColor4ub(0, 144, 0, 194);  // rgb(0,144,0)
  } else if (pp.args.is_conductor) {
    glColor4ub(207, 53, 46, 194);  // orange rgb(207,53,46)
  } else {
    glColor4ub(108, 91, 123, 0);  // blackish
  }
}

void DrawCube(analyzer::nomem pp) {
  GLint x = pp.pwp.x * constants::kCubeSpacing,
        y = pp.pwp.y * constants::kCubeSpacing,
        z = pp.pwp.z * constants::kCubeSpacing;

  glLineWidth(constants::kTinyLineWidth);  // rgb(255,223,0)
  glColor4ub(255, 223, 0, 127);            // orange

  for (size_t i = 0; i < pp.near_.size(); i++) {
    analyzer::point cur = pp.near_[i];

    GLint to_x = cur.pwp.x * constants::kCubeSpacing,
          to_y = cur.pwp.y * constants::kCubeSpacing,
          to_zzz = cur.pwp.z * constants::kCubeSpacing;

    glBegin(GL_LINES);
    glVertex3d(x + static_cast<double>(constants::kCubeSize) / 2,
               y + static_cast<double>(constants::kCubeSize) / 2,
               z + static_cast<double>(constants::kCubeSize) / 2);
    glVertex3d(to_x + static_cast<double>(constants::kCubeSize) / 2,
               to_y + static_cast<double>(constants::kCubeSize) / 2,
               to_zzz + static_cast<double>(constants::kCubeSize) / 2);
    glEnd();
  }

  /*
  glBegin(GL_LINES);
  glVertex3i(x, y, z);
  glVertex3i(x + constants::kCubeSize, y, z);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize, z);
  glVertex3i(x, y + constants::kCubeSize, z);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x, y, z);
  glVertex3i(x + constants::kCubeSize, y, z);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x + constants::kCubeSize, y, z + constants::kCubeSize);
  glVertex3i(x + constants::kCubeSize, y, z);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x, y, z);
  glVertex3i(x, y, z + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x, y, z + constants::kCubeSize);
  glVertex3i(x + constants::kCubeSize, y, z + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize,
             z + constants::kCubeSize);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize, z);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x + constants::kCubeSize, y, z);
  glVertex3i(x + constants::kCubeSize, y, z + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize,
             z + constants::kCubeSize);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize, z);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x + constants::kCubeSize, y, z);
  glVertex3i(x + constants::kCubeSize, y, z + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize,
             z + constants::kCubeSize);
  glVertex3i(x, y + constants::kCubeSize, z + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(x, y + constants::kCubeSize, z);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize, z);
  glEnd();
  */

  ChooseColor(pp);

  glBegin(GL_POLYGON);
  glVertex3i(x, y, z);
  glVertex3i(x + constants::kCubeSize, y, z);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize, z);
  glVertex3i(x, y + constants::kCubeSize, z);
  glEnd();

  glBegin(GL_POLYGON);
  glVertex3i(x, y, z);
  glVertex3i(x + constants::kCubeSize, y, z);
  glVertex3i(x + constants::kCubeSize, y, z + constants::kCubeSize);
  glVertex3i(x, y, z + constants::kCubeSize);
  glEnd();

  glBegin(GL_POLYGON);
  glVertex3i(x, y, z);
  glVertex3i(x, y, z + constants::kCubeSize);
  glVertex3i(x + constants::kCubeSize, y, z + constants::kCubeSize);
  glVertex3i(x + constants::kCubeSize, y, z);
  glEnd();

  glBegin(GL_POLYGON);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize,
             z + constants::kCubeSize);
  glVertex3i(x, y + constants::kCubeSize, z + constants::kCubeSize);
  glVertex3i(x, y, z + constants::kCubeSize);
  glVertex3i(x + constants::kCubeSize, y, z + constants::kCubeSize);
  glEnd();

  glBegin(GL_POLYGON);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize,
             z + constants::kCubeSize);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize, z);
  glVertex3i(x + constants::kCubeSize, y, z);
  glVertex3i(x + constants::kCubeSize, y, z + constants::kCubeSize);
  glEnd();

  glBegin(GL_POLYGON);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize,
             z + constants::kCubeSize);
  glVertex3i(x, y + constants::kCubeSize, z + constants::kCubeSize);
  glVertex3i(x, y + constants::kCubeSize, z);
  glVertex3i(x + constants::kCubeSize, y + constants::kCubeSize, z);
  glEnd();
}

void DrawEdges(uint last_max_z, uint last_max_x, uint last_max_y) {
  glLineWidth(constants::kBigLineWidth);
  glColor4ub(140, 146, 172, 128);  // rgb(140,146,172)

  glBegin(GL_LINES);
  glVertex3i(0, 0, 0);
  glVertex3i(0, 0, (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, 0, 0);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, 0, 0);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, 0, 0);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, 0, (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i(0, 0, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0, 0);
  glVertex3i(0, 0, (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, 0, (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0, 0);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, 0, (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i(0, (last_max_y)*constants::kCubeSpacing + constants::kCubeSize, 0);
  glEnd();

  glBegin(GL_LINES);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0,
             (last_max_z)*constants::kCubeSpacing + constants::kCubeSize);
  glVertex3i((last_max_x)*constants::kCubeSpacing + constants::kCubeSize, 0, 0);
  glEnd();
}

CustomWindow::CustomWindow(int W, int H, const char *str)
    : Fl_Gl_Window(W, H, str) {}

void CustomWindow::InitVector(const vector<analyzer::nomem> v) {
  v_points_ = v;

  size_t temp = v_points_.size();
  analyzer::nomem last_pp = v_points_[temp - 1];

  last_max_x_ = last_pp.pwp.x;
  last_max_y_ = last_pp.pwp.y;
  last_max_z_ = last_pp.pwp.z;

  if (last_pp.pwp.x == 0 || last_pp.pwp.y == 0 || last_pp.pwp.z == 0) {
    camera_x_ = static_cast<float>(0);
    camera_y_ = static_cast<float>(0);
    camera_z_ = static_cast<float>(0);
    camera_pitch_ = 90;
  } else {
    camera_x_ = static_cast<float>(last_max_x_ * 2 * constants::kCubeSpacing);
    camera_y_ = static_cast<float>(last_max_y_ * 2 * constants::kCubeSpacing);
    camera_z_ = static_cast<float>(last_max_z_ / 2 * constants::kCubeSpacing);
    camera_pitch_ = 0;
  }

  camera_yaw_ = static_cast<float>(constants::kM_PI * 5 / 4);
}

/**
 * @brief Move camera according to pitch and yaw.
 */
void CustomWindow::CameraMove(float forward, float right, float up) {
  camera_x_ += forward * cos(camera_yaw_);
  camera_y_ += forward * sin(camera_yaw_);
  camera_z_ += forward * sin(camera_pitch_);

  camera_x_ += right * sin(camera_yaw_);
  camera_y_ -= right * cos(camera_yaw_);

  camera_z_ += up;
}

/**
 * @brief Rotate camera.
 */
void CustomWindow::CameraRotate(float yaw, float pitch) {
  camera_yaw_ += yaw;
  camera_pitch_ += pitch;

  while (camera_yaw_ < 0) {
    camera_yaw_ += static_cast<float>(constants::kM_PI * 2);
  }

  while (camera_yaw_ > constants::kM_PI * 2) {
    camera_yaw_ -= static_cast<float>(constants::kM_PI * 2);
  }

  if (camera_pitch_ > constants::kM_PI / 180 * 89) {
    camera_pitch_ = static_cast<float>(constants::kM_PI / 180 * 89);
  }

  if (camera_pitch_ < -constants::kM_PI / 180 * 89) {
    camera_pitch_ = static_cast<float>(-constants::kM_PI / 180 * 89);
  }
}

void CustomWindow::draw() {
  if (!valid()) {
    // rgb(0,33,71)
    glClearColor(0, 0.13, 0.277, 1);  // cyan
    // enable transparency
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
  }

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60.0, constants::kW / constants::kH, 0.1, 1000.0);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(camera_x_, camera_y_, camera_z_,
            camera_x_ + cos(camera_yaw_) * cos(camera_pitch_),
            camera_y_ + sin(camera_yaw_) * cos(camera_pitch_),
            camera_z_ + sin(camera_pitch_), 0, 0, 1);

  size_t len = v_points_.size();
  for (size_t i = 0; i < len; ++i) {
    DrawCube(v_points_[i]);
  }
  DrawEdges(last_max_z_, last_max_x_, last_max_y_);
}

int CustomWindow::handle(int event) {
  static int prev_mouse_x = 0, prev_mouse_y = 0;

  if (event == FL_KEYUP || event == FL_KEYDOWN) {
    step_ = Fl::event_key(FL_Shift_L) ? 100.0f : 1.0f;
  }

  switch (event) {
    case FL_KEYUP: {
      return 1;
    }

    case FL_KEYBOARD: {
      switch (Fl::event_key()) {
        case 'a':
        case 'A':
        case FL_Left:
          CameraMove(0, -step_, 0);
          break;

        case 'd':
        case 'D':
        case FL_Right:
          CameraMove(0, step_, 0);
          break;

        case 'w':
        case 'W':
        case FL_Up:
          CameraMove(step_, 0, 0);
          break;

        case 's':
        case 'S':
        case FL_Down:
          CameraMove(-step_, 0, 0);
          break;

        case 'q':
        case 'Q':
          CameraMove(0, 0, -step_);
          break;

        case 'e':
        case 'E':
          CameraMove(0, 0, step_);
          break;

        default:
          return 0;
      }

      redraw();
      return 1;
    }

    case FL_MOUSEWHEEL: {
      CameraMove(Fl::event_dy() * step_, 0, 0);
      redraw();

      return 1;
    }

    case FL_LEFT_MOUSE:
    case FL_RIGHT_MOUSE:
    case FL_MIDDLE_MOUSE: {
      prev_mouse_x = Fl::event_x();
      prev_mouse_y = Fl::event_y();

      return 1;
    }

    case FL_DRAG: {
      int cur_x = Fl::event_x(), cur_y = Fl::event_y();

      float dx = static_cast<float>(static_cast<double>(cur_x - prev_mouse_x) *
                                    constants::kM_PI / 180),
            dy = static_cast<float>(static_cast<double>(cur_y - prev_mouse_y) *
                                    constants::kM_PI / 180);

      prev_mouse_x = cur_x;
      prev_mouse_y = cur_y;

      CameraRotate(dx, dy);
      redraw();

      return 1;
    }

    default: {
      return 0;
    }
  }
}

void CustomWindow::setLink(CustomWindow **that) { this->that = that; }

void CustomWindow::hide() {
  *that = nullptr;
  delete this;
}
}  // namespace FLTK_graphics
