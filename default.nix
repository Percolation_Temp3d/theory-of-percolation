{ pkgs ? import <nixpkgs> { } }:
pkgs.clangStdenv.mkDerivation {
  pname = "fltk-3D";
  version = "0.1.0";
  src = ./.;
  buildInputs = with pkgs; [
    # Build tools
    cmake
    ninja
    # Dependencies
    fltk
    libGL
    libGLU
    freeglut
  ];
}
