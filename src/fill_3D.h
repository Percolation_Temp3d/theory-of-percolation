﻿#ifndef SRC_FILL3D_H_
#define SRC_FILL3D_H_

#include <string>
#include <vector>

#include "src/point.h"
#include "src/test_me.h"

namespace analyzer {
typedef unsigned int uint;

class Fill : public tester::ITest {
 private:
  uint height_, width_, depth_;
  size_t V_;
  bool is_filled_ = false, any_possible_full_path_ = true;

  std::vector<point> filled;

 public:
  Fill(coorscope scope, size_t v_fig);

  bool get_availableness() const;
  std::string Log() override;
  std::vector<point> &get_figure_arr();
  void FillFinal(const std::vector<bool> &choices);
};
}  // namespace analyzer

#endif  // SRC_FILL3D_H_
