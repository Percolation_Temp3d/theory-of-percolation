﻿#include "src/randomizer.h"

#include <algorithm>
#include <cstddef>
#include <ctime>
#include <random>

namespace analyzer {
using std::mt19937;
using std::random_device;
using std::vector;

Randomize::Randomize(size_t v_figure, bool seed) {
  seed_ = seed;

  srand(static_cast<unsigned>(time(nullptr)));
  V_ = v_figure;

  indexes_.reserve(V_);
  choices_.reserve(V_);
  for (size_t i = 0; i < V_; ++i) {
    indexes_.push_back(i);
    choices_.push_back(false);
  }
}

void Randomize::Shuffle() {
  random_device rd;
  mt19937 g(rd());

  std::shuffle(indexes_.begin(), indexes_.end(), g);
}

void Randomize::AddRandomize(size_t count, bool shf) {
  if (seed_) {
    srand(static_cast<unsigned>(time(nullptr)));
  }
  if (shf) {
    Shuffle();
  }

  for (size_t i = 0; i < count; ++i) {
    choices_[indexes_[i]] = true;
  }
}

void Randomize::NullRandomize() {
  indexes_.clear();
  fill(choices_.begin(), choices_.end(), false);
}

vector<bool> &Randomize::get_3D_to_1D() { return choices_; }
}  // namespace analyzer
