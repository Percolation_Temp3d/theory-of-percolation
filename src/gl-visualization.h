#ifndef SRC_GL_VISUALIZATION_H_
#define SRC_GL_VISUALIZATION_H_

#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>

#include <string>
#include <vector>

#include "src/point.h"

namespace FLTK_graphics {
typedef unsigned int uint;

class CustomWindow : public Fl_Gl_Window {
 private:
  std::vector<analyzer::nomem> v_points_;

  uint last_max_x_, last_max_y_, last_max_z_;
  float step_ = 3.0f, camera_x_, camera_y_, camera_z_, camera_pitch_,
        camera_yaw_;

  CustomWindow **that;

 public:
  CustomWindow(int W, int H, const char *str);
  void hide();
  void InitVector(const std::vector<analyzer::nomem> v);
  void setLink(CustomWindow **that);

  void CameraMove(float forward, float right, float up);
  void CameraRotate(float yaw, float pitch);
  void draw();

  int handle(int);
};
}  // namespace FLTK_graphics

#endif  // SRC_GL_VISUALIZATION_H_
