﻿#ifndef SRC_PATH_FINDER_H_
#define SRC_PATH_FINDER_H_

#include <math.h>
#include <vcruntime.h>

#include <algorithm>
#include <array>
#include <vector>

#include "src/constants.h"
#include "src/point.h"

namespace analyzer {
typedef unsigned int uint;

class GraphCalculator {
 private:
  size_t x_scope_, y_scope_, z_scope_;
  uint concentration_;
  bool any_possible_full_path_;

  std::vector<point> &adjacency_matrix_;
  std::vector<std::vector<point *>> cluster_ways_;
  std::vector<std::vector<point *> *> perforating_ways_;

 public:
  GraphCalculator(coorscope scope, std::vector<point> &filled,
                  uint concentration, bool pos);

  template <typename T>
  static T *ConvertToPtr(T &obj) {
    // If T is not a pointer, just create one
    return &obj;
  }

  template <typename T>
  static T *ConvertToPtr(T *obj) {
    // If T is already a pointer, just return it
    return obj;
  }

  bool get_n_deep_path() const;

  std::vector<point> &get_final_paths() const;
  std::vector<std::vector<point *>> &get_bfs();
  std::vector<std::vector<point *> *> &get_dfs();

  void ClearFinal();
  void ClearFS();
  void ClearDFS();
  void UnVisited();
  void ClusterRevisit();
  void CreateMatrix(bool should_be_more);
  void FindDeIsland(bool clear);
  void ViaDFS_util(bool clear);
  void FindDeFull(bool clear);

  size_t CalcMidNeighborsN();

  uint BestNToReserve() const;
  int AroundSummarize(point *grp) const;
  int AroundSummarizeAdjuster() const;
  uint CalcSBetween();
  uint CalcLongestPointDistance();

  template <class T>
  uint CalcMid(const std::vector<T> &v) {
    // Calcs mid value of clusters sizes just by summing and dividing the
    // given value method
    if (v.empty()) {
      return 0;
    }

    uint mid = 0, sum = 0;
    size_t island_len = v.size();

    // This method works for both T* and T
    for (size_t i = 0; i < island_len; ++i) {
      sum += ConvertToPtr(v[i])->size();
    }
    if (island_len == 0) {
      return 0;
    }

    mid = sum / island_len;
    if (mid == 0 && sum != 0) {
      mid = 1;
    }

    return mid;
  }

  template <class T>
  uint CalcMHWScore(std::vector<T> v) {
    // Calcs median value by using optimized algorithm with nth_element
    // sorting
    if (v.empty()) {
      return 0;
    }

    size_t middle_itr = v.size() / 2;

    // Use lambda to create another comparing way
    std::nth_element(v.begin(), v.begin() + middle_itr, v.end(),
                     [](const T &a, const T &b) {
                       return ConvertToPtr(a)->size() < ConvertToPtr(b)->size();
                     });

    size_t median = ConvertToPtr(v[middle_itr])->size();

    if (v.size() % 2 == 0) {
      std::nth_element(v.begin(), v.begin() + middle_itr - 1, v.end());
      return (median + ConvertToPtr(v[middle_itr - 1])->size()) / 2;
    } else {
      return median;
    }
  }

  double CalcMidFractalIndex();

  static double CalcImaginaryFigure(double max_distance) {
    double side = sqrt(max_distance);

    return constants::kNumberOfRectangleSides * side;
  }

  static bool CheckIfPointBelongsToPerimeter(const point &p) {
    uint z = p.pwp.z, y = p.pwp.y, x = p.pwp.x, z_symmetry_counter = 0,
         x_symmetry_counter = 0, y_symmetry_counter = 0;

    dimension upper_z = {x, y, z + 1};
    dimension below_z = {x, y, z - 1};
    dimension left_x = {x - 1, y, z};
    dimension right_x = {x + 1, y, z};
    dimension front_y = {x, y + 1, z};
    dimension back_y = {x, y - 1, z};

    for (size_t i = 0; i < p.near_.size(); ++i) {
      if (p.near_[i]->pwp == upper_z || p.near_[i]->pwp == below_z) {
        z_symmetry_counter++;
      } else if (p.near_[i]->pwp == left_x || p.near_[i]->pwp == right_x) {
        x_symmetry_counter++;
      } else if (p.near_[i]->pwp == front_y || p.near_[i]->pwp == back_y) {
        y_symmetry_counter++;
      }
    }

    uint zx = z_symmetry_counter + x_symmetry_counter,
         zy = y_symmetry_counter + z_symmetry_counter,
         xy = x_symmetry_counter + y_symmetry_counter;

    if (zx > constants::kCountIfBelongsToPLine ||
        zy > constants::kCountIfBelongsToPLine ||
        xy > constants::kCountIfBelongsToPLine) {
      return false;
    }

    return true;
  }

  static double LogToBase(double number, double base) {
    return log(number) / log(base);
  }

  static double Calc3DPointDistance(point p1, point p2) {
    return sqrt(
        pow(static_cast<double>(p1.pwp.x) - static_cast<double>(p2.pwp.x), 2) +
        pow(static_cast<double>(p1.pwp.y) - static_cast<double>(p2.pwp.y), 2) +
        pow(static_cast<double>(p1.pwp.z) - static_cast<double>(p2.pwp.z), 2));
  }

  static double CalcLongestPointDistance(const std::vector<point *> &island) {
    double max_distance = 0;
    size_t island_area = island.size();

    for (size_t i = 0; i < island_area; ++i) {
      point *p_from = island[i];

      for (size_t j = i + 1; j < island_area; ++j) {
        point *p_comparator = island[j];

        double current_distance = Calc3DPointDistance(*p_from, *p_comparator);
        if (current_distance > max_distance) {
          max_distance = current_distance;
        }
      }
    }

    return max_distance;
  }

  static double CalcIslandPerimeter(const std::vector<point *> &island) {
    double P = 0;
    size_t island_area = island.size();

    for (size_t i = 0; i < island_area; ++i) {
      point cur = *island[i];

      if (CheckIfPointBelongsToPerimeter(cur)) {
        ++P;
      }
    }

    return P;
  }

  static double CalcFractalIndexPerOne(const std::vector<point *> &island) {
    size_t data_len = island.size();

    if (data_len == 1 || data_len == 2) {
      return 1;
    } else if (data_len != 0) {
      double perimeter = CalcIslandPerimeter(island);
      double max_distance = CalcLongestPointDistance(island);
      double imaginary_perimeter = CalcImaginaryFigure(max_distance);

      double fractal_index;
      if (imaginary_perimeter < perimeter) {
        fractal_index = LogToBase(perimeter, imaginary_perimeter);
      } else {
        fractal_index = LogToBase(imaginary_perimeter, perimeter);
      }

      return fractal_index;
    }

    return 0;
  }
};
}  // namespace analyzer

#endif  // SRC_PATHFINDER_H_
