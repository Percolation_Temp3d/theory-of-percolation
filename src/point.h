﻿#ifndef SRC_POINT_H_
#define SRC_POINT_H_

#include <array>
#include <vector>

namespace analyzer {
typedef unsigned int uint;

struct dimension {
  uint x = 0, y = 0, z = 0;

  bool operator==(const dimension &rhs) {
    if (x == rhs.x && y == rhs.y && z == rhs.z) {
      return true;
    }

    return false;
  }
};

struct coorscope {
  uint x_max = 0, y_max = 0, z_max = 0;
};

struct physics {
  bool is_conductor = false, visited = false, z_layer_metric = false,
       x_layer_metric = false, through_out = false;
};

struct point {
  dimension pwp;
  physics args;

  std::vector<point *> near_;
};

struct nomem {
  dimension pwp;
  physics args;

  std::vector<point> near_;
};
}  // namespace analyzer

#endif  // SRC_POINT_H_
