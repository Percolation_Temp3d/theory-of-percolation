﻿#ifndef SRC_SMARTSLICES_H_
#define SRC_SMARTSLICES_H_

#include <cstddef>
#include <vector>

#include "src/point.h"

namespace analyzer {
template <class T, class U>
T &set_el(std::vector<T> &m_, U i_, U j_, U k_, U xScp, U yScp) {
  return m_[i_ * xScp * yScp + j_ * yScp + k_];
}

template <class T, class U>
const T &get_el(const std::vector<T> &m_, U i_, U j_, U k_, U xScp, U yScp) {
  return m_[i_ * xScp * yScp + j_ * yScp + k_];
}
}  // namespace analyzer

#endif  // SRC_SMARTSLICES_H_
