﻿#ifndef SRC_CONSTANTS_H_
#define SRC_CONSTANTS_H_

namespace constants {
typedef unsigned int uint;

static constexpr uint kNumberOfRectangleSides = 4;

static constexpr uint kMax3DNeighborsCount = 26;
static constexpr uint kMiddle3DNeighborsCount = 17;
static constexpr uint kLow3DNeighborsCount = 11;
static constexpr uint kMinNeighborsCount = 7;

static constexpr uint kMax2DNeighborsCount = 8;
static constexpr uint kMiddle2DNeighborsCount = 5;
static constexpr uint kMin2DNeighborsCount = 3;

static constexpr uint kMax1DNeighborsCount = 2;
static constexpr uint kMin1DNeighborsCount = 1;

static const char *kSeparator = "-";
static const char *KSEQ = "zxy";

static constexpr uint kBetweenLen = 5;
static constexpr uint kCubeSize = 1;
static constexpr uint kCubeSpacing = 5;
static constexpr uint kIndicators = 12;
static constexpr uint kBigLineWidth = 5;
static constexpr uint kTinyLineWidth = 4;

static constexpr double kH = 480;
static constexpr double kW = 640;
static constexpr double kCountIfBelongsToPLine = 2;
static constexpr double kM_PI = 3.14159265358979323846;
static constexpr double kOutputFloatRoundCounter = 2;
}  // namespace constants

#endif  // SRC_CONSTANTS_H_
