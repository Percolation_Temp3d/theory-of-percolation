﻿#include "src/fill_3D.h"

#include <string>

#include "src/constants.h"
#include "src/smartslices.h"

using std::string;
using std::to_string;
using std::vector;

namespace analyzer {
typedef unsigned int uint;

Fill::Fill(coorscope scope, size_t v_fig) {
  height_ = scope.z_max;
  width_ = scope.x_max;
  depth_ = scope.y_max;

  V_ = v_fig;
}

void Fill::FillFinal(const vector<bool> &choices) {
  bool lz = false, lx = false;

  size_t i = 0;
  for (size_t z = 0; z < height_; ++z) {
    for (size_t x = 0; x < width_; ++x) {
      for (size_t y = 0; y < depth_; ++y) {
        dimension p_3d;
        point grp;

        bool flag = false;

        p_3d.x = x;
        p_3d.y = y;
        p_3d.z = z;

        if (choices[i]) {
          flag = true;
        }

        grp.args.is_conductor = flag;
        grp.pwp = p_3d;
        filled.push_back(grp);

        if (flag) {
          lx = true;
        }

        ++i;
      }

      if (lx) {
        set_el(filled, z, x, static_cast<size_t>(0),
               static_cast<size_t>(width_), static_cast<size_t>(depth_))
            .args.x_layer_metric = true;
        lz = true;
      }
    }

    if (lz) {
      set_el(filled, z, static_cast<size_t>(0), static_cast<size_t>(0),
             static_cast<size_t>(width_), static_cast<size_t>(depth_))
          .args.z_layer_metric = true;
    } else {
      any_possible_full_path_ = false;
    }

    lx = false;
    lz = false;
  }

  is_filled_ = true;
}

string Fill::Log() {
  string output;

  if (is_filled_) {
    for (size_t i = 0; i < V_; ++i) {
      output += "<li> (";
      output += to_string(filled[i].pwp.x) + ", " + to_string(filled[i].pwp.y) +
                ", " + to_string(filled[i].pwp.z) + ", ";
      output += to_string(filled[i].args.is_conductor) + "); ";
    }

    output += "\n";
  } else {
    output += constants::kSeparator;
  }

  return output;
}

vector<point> &Fill::get_figure_arr() { return filled; }

bool Fill::get_availableness() const { return any_possible_full_path_; }
}  // namespace analyzer
