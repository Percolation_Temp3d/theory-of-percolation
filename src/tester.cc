﻿#include "src/tester.h"

#include <math.h>

#include <chrono>
#include <iostream>
#include <sstream>
#include <vector>

#include "src/constants.h"
#include "src/fill_3D.h"
#include "src/path-finder.h"
#include "src/point.h"
#include "src/randomizer.h"
#include "src/test_me.h"

namespace theory_of_percolation_tester {
typedef unsigned int uint;

using std::cout;
using std::string;
using std::stringstream;
using std::to_string;
using std::vector;
using std::chrono::high_resolution_clock;

output::output(size_t V, size_t dielectric_count, size_t conductor_count,
               uint mid_bfs_size, uint mid_dfs_size, uint sBetween,
               size_t n_island, size_t n_only_fully_island, uint mhw_bfs_size,
               uint mhw_dfs_size, size_t neighbors_n, double fractal_index,
               string t) {
  put << u8"<h1>";
  put << u8"<b>Результати:</b>";
  put << u8"</h1><ul>";

  txt_arr[0] = u8"Об'єм: " + to_string(V);
  txt_arr[1] =
      u8"Кількість частинок діелектриків: " + to_string(dielectric_count);
  txt_arr[2] =
      u8"Кількість частинок провідників: " + to_string(conductor_count);
  txt_arr[3] =
      u8"Середній розмір звичайних островів: " + to_string(mid_bfs_size);
  txt_arr[4] = u8"Середній об'єм кластерів, які містять наскрізний шлях: " +
               to_string(mid_dfs_size);
  txt_arr[5] = u8"Площа розподілу: " + to_string(sBetween);
  txt_arr[6] = u8"Кількість островів: " + to_string(n_island);
  txt_arr[7] =
      u8"Кількість наскрізних островів: " + to_string(n_only_fully_island);
  txt_arr[8] =
      u8"Медіана розмірів звичайних островів: " + to_string(mhw_bfs_size);
  txt_arr[9] = u8"Медіана розмірів повних островів: " + to_string(mhw_dfs_size);
  txt_arr[10] = u8"Середня кількість сусідів у однієї частинки провідника: " +
                to_string(neighbors_n);
  txt_arr[11] = u8"Фрактальний показник: " +
                to_string(RoundToNDecimalPlaces(
                    fractal_index, constants::kOutputFloatRoundCounter));

  put << u8"<i>Усі характеристики вказані у частинках";

  time_taken << t;
}

void output::set_filled_vector(const vector<analyzer::point> &filled) {
  v_sim = filled;
}

vector<analyzer::point> &output::get_filled_vector() { return v_sim; }

string output::get_output_str() {
  for (size_t i = 0; i < constants::kIndicators; ++i) {
    put << "<li>" << txt_arr[i] << "</li>";
  }
  put << u8"</ul>";

  return put.str();
}

string output::get_time() { return time_taken.str(); }

Runner::Runner() {}

void Runner::RunTest(tester::ITest *inLog, bool out) {
  string outstr = inLog->Log();

  cout << outstr;
}

vector<analyzer::nomem> Runner::ConvertFromPointer() {
  vector<analyzer::nomem> without;
  size_t len = filled_.size();

  for (size_t i = 0; i < len; ++i) {
    analyzer::point cur = filled_[i];
    analyzer::nomem converted;

    size_t close = cur.near_.size();
    for (size_t j = 0; j < close; ++j) {
      converted.near_.push_back(*cur.near_[j]);
    }

    converted.pwp = cur.pwp;
    converted.args = cur.args;

    without.push_back(converted);
  }

  return without;
}

output Runner::GetDefaultRun(uint end_x, uint end_y, uint end_z,
                             uint percolation, bool parallel,
                             bool optimized_graph_algorithm) {
  size_t V = static_cast<size_t>(end_x) * static_cast<size_t>(end_y) *
             static_cast<size_t>(end_z),
         conductor_count = V * percolation / 100,
         dielectric_count = V - conductor_count;

  auto start = high_resolution_clock::now();

  analyzer::coorscope scope = {end_x, end_y, end_z};

  analyzer::Randomize rand_3D(V);
  rand_3D.AddRandomize(conductor_count);

  analyzer::Fill fill_3D(scope, V);
  vector<bool> &ref = rand_3D.get_3D_to_1D();

  fill_3D.FillFinal(ref);
  bool availableness = fill_3D.get_availableness();
  filled_ = fill_3D.get_figure_arr();  // TODO(myself) WTF??? This ain't gonna
                                       // match right?
  analyzer::GraphCalculator graph(scope, filled_, percolation, availableness);

  graph.CreateMatrix(parallel);

  if (optimized_graph_algorithm) {
    graph.ViaDFS_util(false);
  } else {
    graph.FindDeIsland(false);
  }

  graph.FindDeFull(false);

  vector<vector<analyzer::point *>> &ways = graph.get_bfs();
  vector<vector<analyzer::point *> *> &only = graph.get_dfs();
  size_t incomplete_size = ways.size(), through_s_only = only.size(),
         neighbors_n = graph.CalcMidNeighborsN();
  uint sBetween = graph.CalcSBetween(), mid_bfs_size = graph.CalcMid(ways),
       mid_dfs_size = graph.CalcMid(only),
       mhw_bfs_size = graph.CalcMHWScore(ways),
       mhw_dfs_size = graph.CalcMHWScore(only);
  double fractal_index = graph.CalcMidFractalIndex();

  auto stop = high_resolution_clock::now();
  auto duration = DiffTime(start, stop), duration_in_MS = duration / 1000;
  string dur = to_string(duration_in_MS);

  output put(V, dielectric_count, conductor_count, mid_bfs_size, mid_dfs_size,
             sBetween, incomplete_size, through_s_only, mhw_bfs_size,
             mhw_dfs_size, neighbors_n, fractal_index, dur);
  put.set_filled_vector(filled_);
  return put;
  /*
  for (int i = 1; i <= 100; i++) {
    double allk = 0.0;
    for (int j = 1; j <= 100; j++) {
      size_t V = static_cast<size_t>(end_x) * static_cast<size_t>(end_y) *
                 static_cast<size_t>(end_z),
             conductor_count = V * i / 100,
             dielectric_count = V - conductor_count;

      auto start = high_resolution_clock::now();

      analyzer::coorscope scope = {end_x, end_y, end_z};

      analyzer::Randomize rand_3D(V);
      rand_3D.AddRandomize(conductor_count);

      analyzer::Fill fill_3D(scope, V);
      vector<bool> &ref = rand_3D.get_3D_to_1D();

      fill_3D.FillFinal(ref);
      bool availableness = fill_3D.get_availableness();
      filled_ = fill_3D.get_figure_arr();  // TODO(myself) WTF??? This ain't
                                           // gonna match right?
      analyzer::GraphCalculator graph(scope, filled_, percolation,
                                      availableness);

      graph.CreateMatrix(parallel);

      if (optimized_graph_algorithm) {
        graph.ViaDFS_util(false);
      } else {
        graph.FindDeIsland(false);
      }

      graph.FindDeFull(false);

      vector<vector<analyzer::point *>> &ways = graph.get_bfs();
      vector<vector<analyzer::point *> *> &only = graph.get_dfs();
      size_t incomplete_size = ways.size(), through_s_only = only.size(),
             neighbors_n = graph.CalcMidNeighborsN();
      uint sBetween = graph.CalcSBetween(), mid_bfs_size = graph.CalcMid(ways),
           mid_dfs_size = graph.CalcMid(only),
           mhw_bfs_size = graph.CalcMHWScore(ways),
           mhw_dfs_size = graph.CalcMHWScore(only);
      double fractal_index = graph.CalcMidFractalIndex();

      auto stop = high_resolution_clock::now();
      auto duration = DiffTime(start, stop), duration_in_MS = duration / 1000;
      string dur = to_string(duration_in_MS);

      output put(V, dielectric_count, conductor_count, mid_bfs_size,
                 mid_dfs_size, sBetween, incomplete_size, through_s_only,
                 mhw_bfs_size, mhw_dfs_size, neighbors_n, fractal_index, dur);
      put.set_filled_vector(filled_);
      allk += fractal_index;

      if (i == 100) {
        return put;
      }
    }

    allk /= 100.0;
    std::cout << allk << std::endl;
  }
  */
}
}  // namespace theory_of_percolation_tester
