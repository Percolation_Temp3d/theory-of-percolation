﻿#ifndef SRC_TESTER_H_
#define SRC_TESTER_H_

#include <chrono>
#include <cmath>
#include <sstream>
#include <string>
#include <vector>

#include "src/constants.h"
#include "src/point.h"
#include "src/test_me.h"

namespace theory_of_percolation_tester {
typedef unsigned int uint;

struct output {
  std::stringstream put;
  std::stringstream time_taken;

  std::string txt_arr[constants::kIndicators];
  std::vector<analyzer::point> v_sim;

  static inline double RoundToNDecimalPlaces(double value,
                                             double decimal_places) {
    return ceil(value * pow(10, decimal_places)) / pow(10, decimal_places);
  }

  output(size_t V, size_t dielectric_count, size_t conductor_count,
         uint mid_bfs_size, uint mid_dfs_size, uint sBetween, size_t n_island,
         size_t n_only_fully_island, uint mhw_bfs_size, uint mhw_dfs_size,
         size_t neighbors_n, double fractal_index, std::string t);

  std::string get_output_str();
  std::string get_time();

  std::vector<analyzer::point> &get_filled_vector();
  void set_filled_vector(const std::vector<analyzer::point> &filled);
};

template <class T>
auto DiffTime(T t1, T t2) {
  auto duration =
      std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
  return duration;
}

class Runner {
 private:
  std::vector<analyzer::point> filled_;

 public:
  Runner();

  std::vector<analyzer::nomem> ConvertFromPointer();

  void RunTest(tester::ITest *inLog, bool out = true);
  output GetDefaultRun(uint end_x, uint end_y, uint end_z, uint percolation,
                       bool parallel, bool optimized_graph_algorithm);
};
}  // namespace theory_of_percolation_tester

#endif  // SRC_TESTER_H_
